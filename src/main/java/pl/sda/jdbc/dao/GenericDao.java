package pl.sda.jdbc.dao;

import java.sql.SQLException;
import java.util.Map;

public interface GenericDao {
    void insert(String tab, Map<String, Object> columnValueMap) throws SQLException;

    Map<String, Object> find(String tab, Integer id) throws SQLException;

    void update(String tab, Map<String, Object> columnValueMap) throws SQLException;

    void delete(String tab, Integer id) throws SQLException;
}
