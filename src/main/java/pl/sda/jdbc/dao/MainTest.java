package pl.sda.jdbc.dao;

import pl.sda.jdbc.dao.impl.GenericDaoImpl;
import pl.sda.jdbc.dao.impl.UserDaoImpl;
import pl.sda.jdbc.model.Car;
import pl.sda.jdbc.model.GpsDevice;
import pl.sda.jdbc.model.User;
import pl.sda.jdbc.service.RentService;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MainTest {
    public static void main(String[] args) throws SQLException {
        final Properties connectionProps = new Properties();
        connectionProps.put("user", "root");
        connectionProps.put("password", "admin");
        connectionProps.put("serverTimezone", "CET");

        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jp_rent", connectionProps);


        GenericDaoImpl genericDao = new GenericDaoImpl(conn);
        UserDaoImpl userDao = new UserDaoImpl(conn);

        RentService rentService = new RentService(genericDao, userDao);

        Car car = new Car();
        User user = new User();

        car.setPetrol(new BigDecimal(100.5));
        car.setPlateNumber("SI 123 123");
        car.setId(14);

        user.setId(14);
        user.setName("Jan Kowalski");

        GpsDevice gpsDevice = new GpsDevice();
        gpsDevice.setId(14);


        rentService.registerGpsDevice(gpsDevice);
        car.setDeviceGpsId(gpsDevice.getId());
        rentService.registerCar(car);
        rentService.registerUser(user);

        rentService.rentCarAndMove(car, user);

        User carOwner = rentService.findCarOwner(car);

        System.out.printf("Car %s is currently owned by %s%n",
                car.getPlateNumber(), carOwner.getName());

        GpsDevice deviceGpsOfCar = rentService.findDeviceGpsOfCar(car);

        System.out.printf("and is tracked by device id: %d with status %s%n",
                deviceGpsOfCar.getId(), deviceGpsOfCar.getStatus().name());


        conn.close();
    }
}
