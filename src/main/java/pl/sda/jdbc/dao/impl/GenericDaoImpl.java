package pl.sda.jdbc.dao.impl;

import pl.sda.jdbc.dao.GenericDao;

import java.sql.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class GenericDaoImpl implements GenericDao {
    private Connection connection;

    public GenericDaoImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void insert(String tab, Map<String, Object> columnValueMap) throws SQLException {


        //INSERT INTO tablica (kolumna1, kolumna2) VALUES (?,?)


        Set<String> columnNames = columnValueMap.keySet();

        String joinedColumns = String.join(",", columnNames);

        int columnAmount = columnNames.size();


        // ?, ?, ?, ?
        String placeholdersForValues = Stream.generate(() -> "?")
                .limit(columnAmount)
                .collect(Collectors.joining(","));


        final String sql = String.format("INSERT INTO %s (%s) VALUES(%s)",
                tab,
                joinedColumns,
                placeholdersForValues);

        PreparedStatement preparedStatement = connection.prepareStatement(sql);

        final AtomicInteger i = new AtomicInteger();

        columnNames
                .forEach(columnName -> {
                    try {
                        preparedStatement.setObject(i.incrementAndGet(),
                                columnValueMap.get(columnName));
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                });


        preparedStatement.executeUpdate();
    }

    @Override
    public Map<String, Object> find(String tab, Integer id) throws SQLException {
        String sql = String.format("SELECT * FROM %s WHERE id=?", tab);
        PreparedStatement findByIdStatement = connection.prepareStatement(sql);
        findByIdStatement.setInt(1, id);

        ResultSet findByIdResult = findByIdStatement.executeQuery();

        ResultSetMetaData resultSetMetaData = findByIdResult.getMetaData();
        int columnCount = resultSetMetaData.getColumnCount();
        findByIdResult.first();
        Map<String, Object> result = new HashMap<>();
        IntStream.range(1, columnCount + 1)
                .boxed()
                .forEach(

                                i -> {
                                    try {
                                        String columnName = resultSetMetaData.getColumnName(i);
                                        Object columnValue = findByIdResult.getObject(i);

                                        result.put(columnName, columnValue);
                                    } catch (SQLException e) {
                                        throw new RuntimeException(e);
                                    }
                                });
        return result;
    }

    @Override
    public void update(String tab, Map<String, Object> columnValueMap) throws SQLException {





        // sprawdz co sie stanie, jesli nie uzyjemy kopii hashset
        final Set<String> columnNames = new HashSet<>(columnValueMap.keySet());
        columnNames.remove("id");

        final String updateClauseSetPart = columnNames.stream()
                .map(columnName -> String.format("%s = ?", columnName))
                .collect(Collectors.joining(","));


        final String sql = String.format("UPDATE %s SET %s WHERE id = ?",
                tab,
                updateClauseSetPart);

        final PreparedStatement preparedStatement = connection.prepareStatement(sql);

        final AtomicInteger i = new AtomicInteger();

        columnNames
                .forEach(columnName -> {
                    try {
                        preparedStatement.setObject(i.incrementAndGet(),
                                columnValueMap.get(columnName));
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                });

        preparedStatement.setInt(i.incrementAndGet(), (Integer) columnValueMap.get("id"));

        preparedStatement.executeUpdate();
    }

    @Override
    public void delete(String tab, Integer id) throws SQLException {
        final String sql = String.format("DELETE FROM %s WHERE id = ?", tab);
        PreparedStatement deletePreparedStatement = connection.prepareStatement(sql);
        deletePreparedStatement.setInt(1, id);
        deletePreparedStatement.executeUpdate();
    }
}
