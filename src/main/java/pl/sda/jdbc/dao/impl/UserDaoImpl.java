package pl.sda.jdbc.dao.impl;

import java.sql.*;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class UserDaoImpl {
    private final Connection connection;
    private static final String SELECT_SQL = "SELECT * FROM user WHERE car_id=?";

    public UserDaoImpl(Connection connection) {
        this.connection = connection;
    }

    public Map<String, Object> findByCarId(Integer id) throws SQLException {
        PreparedStatement findByCarIdStatement = connection.prepareStatement(SELECT_SQL);
        findByCarIdStatement.setInt(1, id);

        ResultSet findByIdResult = findByCarIdStatement.executeQuery();

        ResultSetMetaData resultSetMetaData = findByIdResult.getMetaData();
        int columnCount = resultSetMetaData.getColumnCount();

        findByIdResult.first();
        return IntStream.range(1, columnCount + 1)
                .boxed()
                .collect(
                        Collectors.toMap(
                                i -> {
                                    try {
                                        return resultSetMetaData.getColumnName(i);
                                    } catch (SQLException e) {
                                        throw new RuntimeException(e);
                                    }
                                },
                                i -> {
                                    try {
                                        return findByIdResult.getObject(i);
                                    } catch (SQLException e) {
                                        throw new RuntimeException(e);
                                    }
                                }));
    }
}
