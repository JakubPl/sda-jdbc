package pl.sda.jdbc.model;

public enum Status {
    READY, ERROR, MOVING
}
