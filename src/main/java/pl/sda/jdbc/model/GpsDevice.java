package pl.sda.jdbc.model;

public class GpsDevice extends BaseModel {
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
