package pl.sda.jdbc.model;

import java.math.BigDecimal;

public class Car extends BaseModel {
    private BigDecimal petrol;
    private String plateNumber;
    private Integer deviceGpsId;

    public BigDecimal getPetrol() {
        return petrol;
    }

    public void setPetrol(BigDecimal petrol) {
        this.petrol = petrol;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public Integer getDeviceGpsId() {
        return deviceGpsId;
    }

    public void setDeviceGpsId(Integer deviceGpsId) {
        this.deviceGpsId = deviceGpsId;
    }
}
