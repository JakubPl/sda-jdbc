package pl.sda.jdbc.model;

import java.math.BigDecimal;

public class User extends BaseModel {
    private String name;
    private Integer carId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }
}
