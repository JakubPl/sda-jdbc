package pl.sda.jdbc.service;

import pl.sda.jdbc.dao.GenericDao;
import pl.sda.jdbc.dao.impl.UserDaoImpl;
import pl.sda.jdbc.model.Car;
import pl.sda.jdbc.model.GpsDevice;
import pl.sda.jdbc.model.Status;
import pl.sda.jdbc.model.User;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class RentService {
    private GenericDao genericDao;
    private UserDaoImpl userDao;

    public RentService(GenericDao genericDao, UserDaoImpl userDao) {
        this.genericDao = genericDao;
        this.userDao = userDao;
    }

    public void registerUser(User user) throws SQLException {
        Map<String, Object> columnValueMap = mapUserToColumnValueMap(user);

        genericDao.insert("user", columnValueMap);

    }

    public void registerGpsDevice(GpsDevice gpsDevice) throws SQLException {
        gpsDevice.setStatus(Status.READY);

        Map<String, Object> columnValueMap = mapGpsDeviceToColumnValueMap(gpsDevice);

        genericDao.insert("device_gps", columnValueMap);
    }

    public void registerCar(Car car) throws SQLException {
        Map<String, Object> columnValueMap = mapCarToColumnValueMap(car);

        genericDao.insert("car", columnValueMap);
    }

    public User findCarOwner(Car car) throws SQLException {
        Map<String, Object> userColumnValueMap = userDao.findByCarId(car.getId());

        String name = (String) userColumnValueMap.get("name");
        Integer carId = (Integer) userColumnValueMap.get("car_id");
        Integer id = (Integer) userColumnValueMap.get("id");

        User user = new User();
        user.setCarId(carId);
        user.setName(name);
        user.setId(id);

        return user;
    }

    public GpsDevice findDeviceGpsOfCar(Car car) throws SQLException {
        Map<String, Object> deviceGpsColumnValueMap = genericDao.find("device_gps", car.getDeviceGpsId());
        Status status = Status.valueOf((String) deviceGpsColumnValueMap.get("status"));
        Integer id = (Integer) deviceGpsColumnValueMap.get("id");

        GpsDevice gpsDevice = new GpsDevice();
        gpsDevice.setStatus(status);
        gpsDevice.setId(id);

        return gpsDevice;
    }

    public void rentCarAndMove(Car car, User user) throws SQLException {
        user.setCarId(car.getId());

        Integer deviceId = car.getDeviceGpsId();

        GpsDevice gpsDevice = new GpsDevice();

        gpsDevice.setId(deviceId);
        gpsDevice.setStatus(Status.MOVING);

        Map<String, Object> carColumnValueMap = mapCarToColumnValueMap(car);
        Map<String, Object> userColumnValueMap = mapUserToColumnValueMap(user);
        Map<String, Object> deviceGpsColumnValueMap = mapGpsDeviceToColumnValueMap(gpsDevice);

        genericDao.update("user", userColumnValueMap);
        genericDao.update("car", carColumnValueMap);
        genericDao.update("device_gps", deviceGpsColumnValueMap);
    }


    private Map<String, Object> mapCarToColumnValueMap(Car car) {
        Map<String, Object> columnValueMap = new HashMap<>();

        columnValueMap.put("plate_number", car.getPlateNumber());
        columnValueMap.put("petrol", car.getPetrol());
        columnValueMap.put("device_gps_id", car.getDeviceGpsId());
        columnValueMap.put("id", car.getId());
        return columnValueMap;
    }
    private Map<String, Object> mapUserToColumnValueMap(User user) {
        Map<String, Object> columnValueMap = new HashMap<>();

        columnValueMap.put("name", user.getName());
        columnValueMap.put("id", user.getId());
        columnValueMap.put("car_id", user.getCarId());
        return columnValueMap;
    }

    private Map<String, Object> mapGpsDeviceToColumnValueMap(GpsDevice gpsDevice) {
        Map<String, Object> columnValueMap = new HashMap<>();

        columnValueMap.put("id", gpsDevice.getId());
        columnValueMap.put("status", gpsDevice.getStatus().name());
        return columnValueMap;
    }
}
