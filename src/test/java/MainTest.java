import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class MainTest {
    @Test
    public void test() throws SQLException {
        String url = "jdbc:h2:mem:test;INIT=runscript from '~/create.sql'\\;runscript from '~/init.sql'";
        Connection conn = DriverManager.getConnection("jdbc:h2:mem:test");
        System.out.println(conn.createStatement().execute("SELECT 1"));
        conn.close();
    }

}
